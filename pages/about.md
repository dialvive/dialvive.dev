---
layout: page
title: About
permalink: /about/
weight: 3
---

# **About Me**

Hi I am **{{ site.author.name }}** :wave:,<br>
I am an ambitious and creative student, committed to my values. I believe in the power of people to make a positive impact in the world. I love to learn new technologies and being part of the software product or service design process. 

<div class="row">
{% include about/skills.html title="Programming Skills" source=site.data.programming-skills %}
{% include about/skills.html title="Programming Languages" source=site.data.programming-languages %}
{% include about/skills.html title="Frameworks" source=site.data.frameworks %}
{% include about/skills.html title="Other Skills" source=site.data.other-skills %}
</div>

<div class="row">
{% include about/timeline.html %}
</div>
