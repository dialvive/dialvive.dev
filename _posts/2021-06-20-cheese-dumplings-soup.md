---
title: Grandma's cheese dumplings soup
tags: [Recipes, Soups]
style: border
color: warning
description: My favourite grandma's soup
---

There were many times in my childhood when I was doing anything in my house, when I suddenly perceived a delightful smell, sometimes I would try to resist and continue to mind my business, but I never resisted and always went to the kitchen to ask "What are you doing? :eyes:". Well, when my grandmother cooked her cheese dumplings soup I instantly recognized the smell and ran to see if I could steal a spoonful or two, I always had to wait for it to be ready, but managed to steal a cheese ball.

![My dear grandmother](/assets/img/grandma.jpg "My dear grandmother")
<p class="caption">
    My dear grandmother
</p>

This soup is not only great for kids, this soup was cooked everytime the family reunited, and it always was the star of the evening.

## Ingredients (Yields 4 portions)

- 1½ liters chicken stock,
  
- 100 grams Chihuahua cheese (You can use other yellow, soft, flavorful cheese),
  
- 1 egg,

- 4 ripe tomatoes, roasted and peeled,

- ½ small onion,

- 2 tablespoons of flour,

- ¼ teaspoon baking soda.

:herb: **Vegetarian & Vegan note:** You can easily switch chicken stock with vegetable stock, dairy cheese with a veggie cheese alternative as long as it melts, and butter with margarine without hydrogenated oils.

## Procedure

1. Have all of the ingredients prepared and measured before starting (mise en place).

2. Liquify the tomatoes and onion.

3. Melt the butter in a cooking pot, and fry the liquified tomatoes and onions.

4. Once the liquid changes color, pour in the chicken stock and boil.

5. In a bowl, put in the cheese, raw egg, flour, baking soda, and salt and pepper to taste. Mix them until a somewhat sticky dough is made. If it is too sticky, add a little more flour.

6. Once the pot is boiling, form little balls (between 3 and 5 centimeters of diameter) of the cheese dough, and put them in the boiling pot one by one.

7. When you finish adding the cheese dough balls into the pot, lower the heat and let the soup simmer for at least 10 minutes.

8. Taste, and adjust salt & pepper.

9. Serve with parsley powder sprinkled on top to decorate.
