---
title: About Pests and Remedies for Vegetables
tags: [Horticulture, Pests]
style: border
color: success
description: My findings about common pests & their remedies for urban horticulture gardens.
---

I've some experience with pests in my garden, the most common have been white flies, mildew, and snails. Luckily I have tested some solutions on several plants and found some that work, and others that failed (some even killed plants). I'll continue to update this page as I find out more solutions to horticulure pests.

If you have  a solution to repel snails please let me know!

Last update: 03/07/21

| Solution | Works Against | Works for | Harmful for |
|:--------:|---------------|------------|-------------|
| [1] Kirkland Environmentally Responsible Dish Soap | White fly, Leaf miner, Cankerworm | Tomato, Zuccini, Soy, Calendula, Green Beans, Blackberry, Genovese Basil, Jalapeño Chili Pepper | Bell Pepper |
| [2] DEOCIL | Powdery Mildew | Zuccini, Jalapeño Chili Pepper | Kumquat |
| [3] Baking Soda | Powdery Mildew | Zuccini | |
| [4] Baking Soda, Vinegar, Soap | Powdery Mildew | Zuccini | Bell Pepper, Kumquat, Tomato |
| [5] Liquified Habanero Chilies | White fly, Leaf miner, Cankerworm | Tomato, Zuccini, Soy, Green Beans, Blackberry, Genovese Basil, Jalapeño Chili Pepper | Kumquat, Calendula |

## Solutions

### [1] Kirkland Environmentally Responsible Dish Soap

Repels pests, and kills fungus.

- Mix 10ml of soap with 90ml of tap water until well combined.

- Spray on top and bottom of leaves, avoid coating completely.

### [2] DEOCIL

Kills fungus.

- Mix 40ml of DEOCIL with 1 liter of tap water until well combined.

- Spray on affected area until coated.

### [3] Baking Soda

Repels pests, and kills fungus. This is easier on plants than [4]

- Mix 1 teaspoon of baking soda with 1 liter of tap water until well combined.

- Spray on affected area until well coated.

### [4] Baking Soda, Vinegar, Soap

Repels pests, and kills fungus.

- Mix 1 teaspoon of baking soda, 1 teaspoon of white vinegar, and 1 teaspoon of biodegradable Dish Soap with 1 liter of tap water until well combined.

- Spray on affected area until coated.

### [5] Liquified Habanero Chilies

Repels pests, animals, and people!

- Liquify 4 or more habanero chilies with 100ml of tap water, strain with fine mesh strainer and put on a sprayer bottle.

- Wear a face mask and goggles or protective eyewear.

- Spray on top and bottom of leaves until coated.

- Wash your hands and exposed skin (better take a bath).

- Solution lasts for 2 days outside of a fridge.