---
title: Asian spicy noodles
tags: [Recipes, Pasta]
style: border
color: warning
description: My take on Korean spicy noodles with veggies.
---

Three days ago I was in my local supermarket's pasta aisle, I was remembering the Parasite movie which I absolutely loved, and got isnpired with the Chapaguri Ram-Dom noodles they prepared (I had to duck-duck-go it to remember the name). I had never prepared Korean noodles before, but I improvised and got a really good recipe. Note: This recipe is completely different of Chapaguri Ram-Dom noodles.

![My dear grandmother](/assets/img/chapaguri.jpg "Parasite: a great 2019's movie")
<p class="caption">
    Parasite: a great 2019's movie
</p>

This noodles are spicy, but you can adjust to your liking and still get a wonderful dish. Note that the noodles I used included a chicken stock sachet and a dry condiments sachet, but you can easily replace them

## Ingredients (Yields 2 big portions)

- ½ liters of water,
  
- 2 ramen noodle packages (I used Ottogi chicken flavored instant noodles),
  
- 1 chicken stock powder sachet (Included in the noodle packages, replace with 1 teaspoon of chicken stock powder),

- 2 dry condiment sachets (Included in the noodle packages, replace with 10gr diced musrooms and 10gr diced cabbage, preferrably dried),

- ¼ white onion, diced in small cubes,

- ½ red pepper, diced in small cubes,

- ½ green pepper, diced in small cubes,

- ½ head of broccoli, diced in small cubes,

- 1 carrot, peeled, diced in small cubes,

- 1 cup of green beans (replace with edamame),

- 1 white garlic clove, pureed / pressed / diced very small,

- 1½ tablespoon of Szechuan sauce (replace with Sriracha or Hoisin sauce),

- 20ml soy sauce,

- 2 tablespoons of rice vinegar (replace with 1 tablespoon of white or apple cider vinegar),

- 1 tablespoon of sugar,

- 1 lemon's juice,

:herb: **Vegetarian & Vegan note:** You can easily switch chicken stock with vegetable stock. Noodles typically contain eggs, you could search for vegan noodles.

## Procedure

1. Have all of the ingredients prepared and measured before starting (mise en place).

2. Put the water to boil in a pot the size of the noodle packages, when the water starts boiling add the chicken stock powder, dry condiments, and noodles (The noodles should be covered by the water). Simmer for 4 minutes or until noodles are nearly cooked.

3. Meanwhile, in a big pan fry the onion with the Szechuan sauce until golden, add the garlic, peppers, broccoli, carrot, and green beans, and stir fry on high heat for a minute.

4. Pour the noodles with the remaining chicken stock in the pan; mix the soy sauce with the rice vinegar and sugar until completely dissolved and add to the pan.

5. Mix the contents of the pan until combined, be careful not to break the noodles. Continue simmering the remaining liquids in the pan until reduced (thickened and scarce), be careful not to overcook the vegetables (they should remain colorful).

6. Remove from the heat and add the lemon's juice.

7. Serve in bowls with one or two lemon wedges aside.
