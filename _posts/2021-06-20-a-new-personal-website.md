---
title: A new personal website
tags: [Personal]
style: border
color: primary
description: Yet another software developer personal website/blog
---

Well, here am I writing the first entry into my blog. What should you expect? Nothing in particular, I'll write sometimes about software development, tutorials, my personal kitchen recipes, experiences and updates on my horticulture adventures, and othertimes some general thoughts.

If we haven't met, let me introduce myself (although I find this a little awkward since I don't expect people on the internet to be interested in me... at least just yet): I'm currently a computer science student, I enjoy programming as much as product design, recently I have been working on [HandsApp.org](https://handsapp.org).

Cooking is one of my hobbies: I find french, italian, and mexican cuisines really inspiring for everyday cooking. I've been trying to become vegan sporadically for some time now, but tacos, tuna sushi, shrimps, and cheese have really made this a difficult misson.

As you may have noticed (or not), I'm also a horticulture hobbyist. I started out planting zuccini plants (which yielded about 10 zuccinis before a fungi plague struck). I find it specially insightful to care for a living being that produces food from the ground. Also, home grown veggies and friuts are a lot more juicy and flavourful than store bought.

Depending on the day: my favourite color is black or yellow, and  my favorite dessert is ice-cream or date pie.

I think that's it for the meantime. Please leave a comment if you feel like it! :wink:
